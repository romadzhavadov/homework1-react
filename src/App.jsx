import React, { useState } from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import './App.scss';

function App() {

const [header, setHeader] = useState('Do you want ro  delete this file?')
const [text, setText] = useState(`Once you delete this file, it wan\`t be possible to undo this action. \nAre yo sure you want to delete it?`)
const [isCloseButton, setIsCloseButton] = useState(true)
const [modal, setModal] = useState(false)

const clickFirst = (e) => {
  if(!modal) {
    setModal(true)
  } else {
    setModal(false)
  }
} 

const clickSecond = (e) => {
  if(!modal) {
    setModal(true)
    setHeader('Do you want exit?')
    setText('You must confirm that you agree go away from this Modal!')
  } else {
    setModal(false)
  }
 } 

const closeBtnFunction = () => {
  if(isCloseButton) {
    setIsCloseButton(false)
  } else {
    setIsCloseButton(true)
  }
}

  return (
    <div className="container">
      <Button onClick={clickFirst} >Open first modal</Button>
      <Button onClick={clickSecond} name="secondary">Open second modal</Button>
      {modal ? <Modal header={header} text={text} isCloseButton={closeBtnFunction} actions={
        <>
        <button className='btnActions' onClick={clickFirst}>Ok</button>
        <button className='btnActions' onClick={clickFirst}>Cancel</button>
        </>
      } closefunction={clickFirst}/> : null}

      {modal ? <Modal header={header} text={text} isCloseButton={closeBtnFunction} actions={
        <>
        <button className='btnActions' onClick={clickSecond}>Cancel</button>
        </>
      } closefunction={clickSecond}/> : null}
    </div>
  )
}



export default App;
