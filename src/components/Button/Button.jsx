import React from "react";
import styles from './Button.module.scss'
import classNames from 'classnames'

const Button = (props) => {

const { children, name, onClick } = props;

    if(name === "secondary") {
        return(
            <button style={{backgroundColor: '#acaf4c'}} onClick={onClick} className={classNames(styles.btn, styles.btnSecond )}>{children}</button>
        )
    }

    return(
        <button style={{backgroundColor: '#4CAF50'}} onClick={onClick} className={classNames(styles.btn)}>{children}</button>
    )
}

export default Button;