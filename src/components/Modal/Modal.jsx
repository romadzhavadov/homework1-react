import React from "react";
import styles from './Modal.module.scss'


const Modal = (props) => {

    const { header, isCloseButton, text, actions, closefunction} = props;

    console.log(actions)

    return(
        <div className={styles.wrapper}>
            <div className={styles.modalBackground} onClick={closefunction}></div>
            <div className={styles.modal}>
                <div className={styles.modalHeaderContainer}>
                    <h2 className={styles.modalTitle}>{header}</h2>
                   {isCloseButton ? <span className={styles.closeModalButton} onClick={closefunction}>&times;</span> : null} 
                </div>
                <p className={styles.modalContent}>{text}</p>
                {actions}
            </div>
        </div>

    )
}

export default Modal;